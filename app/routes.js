const express = require('express');
const coursesController = require('./controllers/CoursesController');
const studentsController = require('./controllers/StudentsController');
const studentCoursesController = require('./controllers/StudentCoursesController');

const router = express.Router();

router.post('/courses', coursesController.createCourses);
router.get('/courses', coursesController.findAll);
router.get('/courses/:courseId', coursesController.findById);
router.put('/courses/:courseId', coursesController.updateCourse);
router.delete('/courses/:courseId', coursesController.delete);

router.post('/students', studentsController.createStudents);
router.get('/students', studentsController.findAll);
router.get('/students/:studentId', studentsController.findById);
router.put('/students/:studentId', studentsController.updateStudent);
router.delete('/students/:studentId', studentsController.delete);

router.post('/student/course', studentCoursesController.assignCourse);
router.get('/student/course/:studentId', studentCoursesController.findByStudent);
router.get('/student/not-course/:studentId', studentCoursesController.findCoursesNotStudent);
router.delete('/student/course/:studentId/:idCourse', studentCoursesController.delete);

module.exports = router;
