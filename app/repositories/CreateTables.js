const CreateTables = module.exports
const { Student } = require('../models/Student.js')
const { Course } = require('../models/Course.js')
const { StudentCourse } = require('../models/StudentCourse.js')

CreateTables.create = async function createTables() {
    try {
        await Student.sync({force: true});
        await Course.sync({force: true});
        await StudentCourse.sync({force: true});
    }catch (e) {
        console.log(e)
    }
}