const CoursesRepository = module.exports;
const connection = require('../configs/Database.js')

const {Course} = require('../models/Course.js');

CoursesRepository.createCourses = async (course) => Course.create(course);

CoursesRepository.findAll = async () => Course.findAll();

CoursesRepository.findById = async (courseId) => {
    return Course.findAll({
        where: {
            id: courseId
        }
    });
}

CoursesRepository.findByIds = async (courseIds) => {
    return Course.findAll({
        where: {
            id: {
                [connection.Op.in]: courseIds
            }
        }
    });
}

CoursesRepository.updateCourse = async (courseId, course) => {
    return Course.update(course, {
        where: {
            id: courseId
        }
    });
}

CoursesRepository.delete = async (courseId) => {
    return Course.destroy({
        where: {
            id: courseId
        }
    });
}