const StudentsRepository = module.exports;

const {Student} = require('../models/Student.js');

StudentsRepository.createStudent = async (studentRequest) => {
    return Student.create(studentRequest);
}

StudentsRepository.findAll = async () => {
    return Student.findAll();
}

StudentsRepository.findById = async (studentId) => {
    return Student.findAll({
        where: {
            id: studentId
        }
    });
}

StudentsRepository.updateStudent = async (studentId, student) => {
    return Student.update(student, {
        where: {
            id: studentId
        }
    });
}

StudentsRepository.delete = async (studentId) => {
    return Student.destroy({
        where: {
            id: studentId
        }
    });
}