const StudentCoursesRepository = module.exports;

const {StudentCourse} = require('../models/StudentCourse.js');
const connection = require('../configs/Database.js')
const {Course} = require("../models/Course.js");

StudentCoursesRepository.assignCourse = async (studentCourseRequest) => {
    return StudentCourse.create(studentCourseRequest);
}

StudentCoursesRepository.findByStudentAndCourse = async (studentId, courseId) => {
    return StudentCourse.findAll({
        where: {
            [connection.Op.and]: [
                { IdEstudiante: studentId },
                { IdCurso: courseId }
            ]
        }
    });
}

StudentCoursesRepository.findByStudent = async (studentId) => {
    return StudentCourse.findAll({
        where: {
            IdEstudiante: studentId
        }
    });
}

StudentCoursesRepository.findCoursesNotStudent = async (studentId) => {
        return StudentCourse.findAll({
            where: {
                IdEstudiante: {
                    [connection.Op.ne]: studentId
                }
            }
        });
}

StudentCoursesRepository.delete = async (idCourse, studentId) => {
    return StudentCourse.destroy({
        where: {
            IdCurso: idCourse,
            IdEstudiante: studentId
        }
    });
}
