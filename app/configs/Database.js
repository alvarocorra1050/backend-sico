const {Sequelize, DataTypes, Op} = require('sequelize')

const sequelize = new Sequelize('pruebasico', 'sa', '123456', {
    dialect: 'mssql',
    port: 63837,
    host: 'localhost'
});

exports.DataTypes = DataTypes
exports.sequelize = sequelize
exports.Op = Op