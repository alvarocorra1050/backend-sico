const CoursesController = module.exports;

const CoursesService = require('../services/CoursesService.js');
const Validator = require('../utils/Validator');
const CourseScheme = require('../models/requests/CourseScheme');

CoursesController.createCourses = (req, res, next) => {
    const section = 'CoursesController.createCourses';
    const { body } = req;

    console.log('starts' + section + JSON.stringify(body));

    Validator(CourseScheme).validateRequest(body);

    return CoursesService.createCourse(body)
        .then((data) => {
            console.log(section + 'ends with' + JSON.stringify(body)+ 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

CoursesController.findAll = (req, res, next) => {
    const section = 'CoursesController.findAll';

    console.log('starts' + section);

    return CoursesService.findAll()
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

CoursesController.findById = (req, res, next) => {
    const section = 'CoursesController.findById';
    const { params: { courseId } } = req;

    console.log('starts' + section);

    return CoursesService.findById(courseId)
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

CoursesController.updateCourse = (req, res, next) => {
    const section = 'CoursesController.updateCourse';
    const { params: { courseId }, body } = req;

    console.log('starts' + section);

    return CoursesService.updateCourse(courseId, body)
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

CoursesController.delete = (req, res, next) => {
    const section = 'CoursesController.delete';
    const { params: { courseId } } = req;

    console.log('starts' + section);

    return CoursesService.delete(courseId)
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.sendStatus(200);
        })
        .catch(error => next(error));
};