const StudentCoursesController = module.exports;

const StudentCoursesServices = require('../services/StudentCoursesServices');
const Validator = require('../utils/Validator');
const StudentsCourseScheme = require('../models/requests/StudentsCourseScheme');

StudentCoursesController.assignCourse = (req, res) => {
    const section = 'StudentCoursesController.assignCourse';
    const {body} = req;

    console.log('starts' + section + JSON.stringify(body));

    Validator(StudentsCourseScheme).validateRequest(body);

    return StudentCoursesServices.assignCourse(body)
        .then((data) => {
            console.log(section + 'ends with' + JSON.stringify(body) + 'successfully');

            return res.send(data);
        })
        .catch(error => {
            return res.status(400).send({error: error});
        });
};

StudentCoursesController.findByStudent = (req, res, next) => {
    const section = 'StudentCoursesController.findByStudent';
    const { params: { studentId } } = req;

    console.log('starts' + section);

    return StudentCoursesServices.findByStudent(studentId)
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

StudentCoursesController.findCoursesNotStudent = (req, res, next) => {
    const section = 'StudentCoursesController.findCoursesNotStudent';
    const { params: { studentId } } = req;

    console.log('starts' + section);

    return StudentCoursesServices.findCoursesNotStudent(studentId)
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

StudentCoursesController.delete = (req, res, next) => {
    const section = 'StudentCoursesController.delete';
    const { params: { idCourse, studentId } } = req;

    console.log('starts' + section);

    return StudentCoursesServices.delete(idCourse, studentId)
        .then((data) => {
        console.log(section + 'ends with' + data + 'successfully');

        return res.send(data);
    })
        .catch(error => {
            return res.status(400).send({error: error});
        });
};
