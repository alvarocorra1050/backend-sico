const StudentsController = module.exports;

const StudentsService = require('../services/StudentsService');
const Validator = require('../utils/Validator');
const StudentScheme = require('../models/requests/StudentScheme');


StudentsController.createStudents = (req, res, next) => {
    const section = 'StudentsController.createStudents';
    const {body} = req;

    console.log('starts' + section + JSON.stringify(body));

    Validator(StudentScheme).validateRequest(body);

    return StudentsService.createStudent(body)
        .then((data) => {
            console.log(section + 'ends with' + JSON.stringify(body) + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

StudentsController.findAll = (req, res, next) => {
    const section = 'StudentsController.findAll';

    console.log('starts' + section);

    return StudentsService.findAll()
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

StudentsController.findById = (req, res, next) => {
    const section = 'StudentsController.findById';
    const { params: { studentId } } = req;

    console.log('starts' + section);

    return StudentsService.findById(studentId)
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

StudentsController.updateStudent = (req, res, next) => {
    const section = 'StudentsController.updateStudent';
    const { params: { studentId }, body } = req;

    console.log('starts' + section);

    return StudentsService.updateStudent(studentId, body)
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => next(error));
};

StudentsController.delete = (req, res) => {
    const section = 'StudentsController.delete';
    const { params: { studentId } } = req;

    console.log('starts' + section);

    return StudentsService.delete(studentId)
        .then((data) => {
            console.log(section + 'ends with' + data + 'successfully');

            return res.send(data);
        })
        .catch(error => {
            return res.status(400).send({error: error});
        });
};
