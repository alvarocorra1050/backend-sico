const CoursesService = module.exports;
const courseRepository = require('../repositories/CoursesRepository');

CoursesService.createCourse = async (course) => courseRepository.createCourses(course);

CoursesService.findAll = async () => courseRepository.findAll();

CoursesService.findById = async (courseId) => {

    return courseRepository.findById(courseId);
};

CoursesService.updateCourse = async (courseId, course) => {

    return courseRepository.updateCourse(courseId, course);
};

CoursesService.delete = async (courseId) => {

    return courseRepository.delete(courseId);
};