const StudentsService = module.exports;
const studentsRepository = require('../repositories/StudentsRepository');
const studentCoursesRepository = require('../repositories/StudentCoursesRepository');

StudentsService.createStudent = async (student) => {

    const studentResponse = await studentsRepository.createStudent(student);

    const {dataValues} = studentResponse

    return dataValues
};

StudentsService.findAll = async () => {

    return studentsRepository.findAll();
};

StudentsService.findById = async (studentId) => {

    return studentsRepository.findById(studentId);
};

StudentsService.updateStudent = async (studentId, student) => {

    return studentsRepository.updateStudent(studentId, student);
};

StudentsService.delete = async (studentId) => {

    const studentCourse = await studentCoursesRepository.findByStudent(studentId);

    if (studentCourse.length === 0){
        await studentsRepository.delete(studentId);

        return {message: 'Se elimino el estudiante con id '+ studentId + ' correctamente.'}
    }

    throw {message: 'El estudiante no se puede eliminar por que tiene asignado un curso'};
};