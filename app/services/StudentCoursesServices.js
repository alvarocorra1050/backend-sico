const StudentCoursesServices = module.exports;
const studentCoursesRepository = require('../repositories/StudentCoursesRepository');
const coursesRepository = require('../repositories/CoursesRepository');

StudentCoursesServices.assignCourse = async (studentCourse) => {

    const {IdEstudiante: studentId, IdCurso: courseId} = studentCourse;

    const validateCourse = await studentCoursesRepository.findByStudentAndCourse(studentId, courseId)

    if (validateCourse.length === 0) {
        return studentCoursesRepository.assignCourse(studentCourse)
    }

    throw {message: 'El estudiante ya tiene asignado este curso'};
};

StudentCoursesServices.findByStudent = async (studentId) => {
    const ids = []
    const assignedCourses = await studentCoursesRepository.findByStudent(studentId);

    for (let i = 0; i < assignedCourses.length; i++) {
        ids.push(assignedCourses[i].dataValues.IdCurso)
    }

    return coursesRepository.findByIds(ids)
}

StudentCoursesServices.findCoursesNotStudent = async (studentId) => {
    const ids = []
    const idsAllCourses = []
    let assignedCourses = await studentCoursesRepository.findByStudent(studentId);

    for (let i = 0; i < assignedCourses.length; i++) {
        ids.push(assignedCourses[i].dataValues.IdCurso)
    }

    let allCourses = await coursesRepository.findAll();

    for (let i = 0; i < allCourses.length; i++) {
        idsAllCourses.push(allCourses[i].dataValues.Id)
    }

    ids.filter((course) => {
        removeItemFromArr(idsAllCourses, course)
    })

    return coursesRepository.findByIds(idsAllCourses)
}

function removeItemFromArr (arr, item ) {
    const i = arr.indexOf(item);

    if ( i !== -1 ) {
        arr.splice( i, 1 );
    }
}

StudentCoursesServices.delete = async (idCourse, studentId) => {
    await studentCoursesRepository.delete(idCourse, studentId);

    return {message: 'El estudiante ya no esta asignado a este curso'}
}
