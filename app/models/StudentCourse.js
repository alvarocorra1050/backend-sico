const connection = require('../configs/Database.js')

const StudentCourse = connection.sequelize.define('EstudianteCurso', {
    Id: {
        type: connection.DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    IdEstudiante: {
        type: connection.DataTypes.STRING,
        allowNull: false
    },
    IdCurso: {
        type: connection.DataTypes.INTEGER,
        allowNull: false
    },
    NotaFinal: {
        type: connection.DataTypes.NUMBER,
        allowNull: true
    }
},{
    tableName: 'EstudianteCurso',
    createdAt: false,
    updatedAt: false,
    timestamps: false
})

exports.StudentCourse = StudentCourse