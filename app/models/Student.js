const connection = require('../configs/Database.js')

const Student = connection.sequelize.define('Estudiante', {
    Id: {
        type: connection.DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    TipoIdentificacion: {
        type: connection.DataTypes.STRING,
        allowNull: false
    },
    Identificacion: {
        type: connection.DataTypes.STRING,
        allowNull: false
    },
    Nombre1: {
        type: connection.DataTypes.STRING,
        allowNull: false
    },
    Nombre2: {
        type: connection.DataTypes.STRING,
        allowNull: true
    },
    Apellido1: {
        type: connection.DataTypes.STRING,
        allowNull: false
    },
    Apellido2: {
        type: connection.DataTypes.STRING,
        allowNull: true
    },
    Email: {
        type: connection.DataTypes.STRING,
        allowNull: true
    },
    Celular: {
        type: connection.DataTypes.STRING,
        allowNull: true
    },
    Direccion: {
        type: connection.DataTypes.STRING,
        allowNull: true
    },
    Ciudad: {
        type: connection.DataTypes.STRING,
        allowNull: false
    }
},{
    tableName: 'Estudiante',
    createdAt: false,
    updatedAt: false,
    timestamps: false
});

exports.Student = Student