const connection = require('../configs/Database.js')

const Course = connection.sequelize.define('Curso', {
    Id: {
        type: connection.DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    Nombre: {
        type: connection.DataTypes.STRING,
        allowNull: false
    },
    NumeroCreditos: {
        type: connection.DataTypes.INTEGER,
        allowNull: false
    }
},{
    tableName: 'Curso',
    createdAt: false,
    updatedAt: false,
    timestamps: false
})

exports.Course = Course