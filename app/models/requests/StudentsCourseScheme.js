module.exports = {
    title: 'StudentsCourseCreate',
    type: 'object',
    properties: {
        IdEstudiante: {
            type: 'integer',
        },
        IdCurso: {
            type: 'integer',
        },
        NotaFinal: {
            type: 'number',
        }
    },
    required: ['IdEstudiante', 'IdCurso'],
};
