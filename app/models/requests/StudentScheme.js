module.exports = {
    title: 'StudentCreate',
    type: 'object',
    properties: {
        TipoIdentificacion: {
            type: 'string',
        },
        Identificacion: {
            type: 'string',
        },
        Nombre1: {
            type: 'string',
        },
        Nombre2: {
            type: 'string',
        },
        Apellido1: {
            type: 'string',
        },
        Apellido2: {
            type: 'string',
        },
        Email: {
            type: 'string',
        },
        Celular: {
            type: 'string',
        },
        Direccion: {
            type: 'string',
        },
        Ciudad: {
            type: 'string',
        }
    },
    required: ['TipoIdentificacion', 'Identificacion', 'Nombre1', 'Apellido1', 'Ciudad'],
};
