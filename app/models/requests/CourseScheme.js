module.exports = {
    title: 'StudentCreate',
    type: 'object',
    properties: {
        Nombre: {
            type: 'string',
        },
        NumeroCreditos: {
            type: 'integer',
        }
    },
    required: ['Nombre', 'NumeroCreditos'],
};
