# BackendSico

Version de npm es `6.14.8` y la version de node es `v14.15.0`
# Instalando dependencias

Ejecutar el comando `npm i` para instalar dependencias.

# Cadena de conexion a la BDs

Nos ubicamos en el archivo `app/configs/Database.js` alli colocamos nuestros datos.

`const sequelize = new Sequelize('Nombre base de datos', 'usuario', 'contraseña', {
  dialect: 'mssql',
  port: 63837,
  host: 'localhost'
});`

# Scripts para agregar informacion a la Bds (Opcional)

En la carpeta `scripts` encontraremos dos archivos *.sql con esto podemos nutrir las tablas de algunos datos.

# Correr servidor

Verificar que el servidor de lado backend se este ejecutando

Ejecutar el comando `npm start` ya podemos ejecutar endpoints con esta URL como base `http://localhost:3000`.